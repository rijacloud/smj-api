<?php

namespace App\Providers;

use App\Models\Account;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Account::creating(function($user) {
            $user->uniqid = uniqid('scz_');
            return true;
        });
    }
}
