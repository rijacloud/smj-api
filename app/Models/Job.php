<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    use HasFactory;

    protected $table = "Jobs";

    public function categories() {
        return $this->belongsToMany('App\Models\Category','job_category');
    }

    public function remise() {
        return $this->hasMany('App\Models\Remise');
    }

    public function offres() {
        return $this->hasMany('App\Models\Offre');
    }

    public function curl_get_content($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $html = curl_exec($ch);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
    /*
    public function getLogoAttribute($value) {
        return base64_encode($this->curl_get_content(url('storage/'.$value)));
    }
    */

}
