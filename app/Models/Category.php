<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Category extends Model
{
    use HasFactory;

    protected $table = "categories";

    public function jobs() {
        return $this->belongsToMany('App\Models\Job','job_category');
    }
    
    public function getLabelAttribute($value) {
        return Str::slug($value);
    }
}
