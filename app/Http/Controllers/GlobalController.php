<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Account;
use App\Models\Job;
use App\Models\Category;

class GlobalController extends Controller
{
    public function index(Request $request, $id = "") {
        if($id == "") {
            //$request->session()->forget('url');
            return view('template', compact('id'));
        } else {
            //redirect to the url here
            //update increment
            $account = Account::where('uniqid',$id)->first();
            
            if($account) {
                if($account->url != "" && $account->url != null) {
                    $account->views = ($account->views == null) ? 1 :  $account->views + 1 ;
                    $account->save();
                    return redirect('https://instagram.com/'. $account->url);
                } else {
                    return view('template', compact('id'));
                }
            } else {
                $request->session()->flash('invalid','Sorry the QrCode you are trying to use do not exist');
                return view('template', 'id');
            }
        }
    }

    public function create(Request $request) {
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:accounts,url|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('scan')
                        ->withErrors($validator)
                        ->withInput();
        }

        $id = $request->get('uid');

        if($id) {
            $account = Account::where('uniqid',$id)->first();
            $account->url = $request->get('username');
            $account->save();
        } else {
            $account = new Account();
            $account->url = $request->get('username');
            $account->save();
        }

        $request->session()->flash('status','Your Qr Code is now active, you can share it around !!');
        $request->session()->flash('url',$account->uniqid);

        return redirect('scan');
    }

    public function jobs() {
        $all = Category::with('jobs')->with('jobs.categories')->get();
        $jobs = Job::with('categories','remise','offres')->get()->toArray();
        return response()->json(compact('all','jobs'));
    }
}
